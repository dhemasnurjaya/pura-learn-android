package com.dhemas.app.android101;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private EditText input_nik;
    private EditText input_nama;
    private ListView listView;

    public static SQLiteDatabase sqLiteDatabase;
    public SqliteHandler sqliteHandler;

    public static final ArrayList<Pegawai> listPegawai = new ArrayList();
    public static PegawaiAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //initialize views
        input_nik = (EditText) findViewById(R.id.input_nik);
        input_nama = (EditText) findViewById(R.id.input_nama);
        listView = (ListView) findViewById(R.id.listview);

        sqliteHandler = new SqliteHandler(MainActivity.this);
        sqLiteDatabase = sqliteHandler.getWritableDatabase();

        getData();
        bindData();
    }

    public void clickAction(View view) {
        switch (view.getId()){
            case R.id.button:
                String nik = input_nik.getText().toString();
                String nama = input_nama.getText().toString();

                //sqlite insert
                ContentValues values = new ContentValues();
                values.put(SqliteHandler.COL_NIK, nik);
                values.put(SqliteHandler.COL_NAMA, nama);

                long id = sqLiteDatabase.insert(SqliteHandler.TBNAME, null, values);
                Log.i("id", id + "");
                if(id > 0){
                    //list insert
                    listPegawai.add(new Pegawai(nik, nama));
                    adapter.notifyDataSetChanged();

                    input_nik.setText("");
                    input_nama.setText("");
                } else {
                    Toast.makeText(this, "Constraint violation", Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.button_clear:
                clearData();
                listPegawai.clear();
                adapter.notifyDataSetChanged();

            default: break;
        }
    }

    public void getData(){
        String[] projection = {sqliteHandler.COL_NIK, sqliteHandler.COL_NAMA};
        Cursor cursor = sqLiteDatabase.query(sqliteHandler.TBNAME, projection, null, null, null, null, null);

        if(cursor.moveToFirst()){
            do {
                Pegawai pegawai = new Pegawai(cursor.getString(0), cursor.getString(1));
                listPegawai.add(pegawai);
            } while (cursor.moveToNext());
        }
    }

    public void clearData(){
        sqLiteDatabase.delete(SqliteHandler.TBNAME, null, null);
    }

    public void bindData(){
        adapter = new PegawaiAdapter(this, listPegawai);
        listView.setAdapter(adapter);
    }

    @Override
    protected void onDestroy() {
        sqliteHandler.close();
        super.onDestroy();
    }
}
