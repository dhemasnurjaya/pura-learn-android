package com.dhemas.app.android101;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Android101 by dhemas
 * created on: 29/03/2017
 */

public class SqliteHandler extends SQLiteOpenHelper {

    private static final String DBNAME = "mydatabase";
    private static final int DBVERSION = 1;

    public static final String TBNAME = "TB_PEGAWAI";
    public static final String COL_NIK = "NIK";
    public static final String COL_NAMA = "NAMA";

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + TBNAME + " (" +
                    COL_NIK + " TEXT PRIMARY KEY," +
                    COL_NAMA + " TEXT" +
                ")";

    private static final String SQL_DROP_ENTRIES =
            "DROP TABLE IF EXISTS " + TBNAME;


    public SqliteHandler(Context context) {
        super(context, DBNAME, null, DBVERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
