package com.dhemas.app.android101;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

/**
 * Android101 by dhemas
 * created on: 30/03/2017
 */

public class DialogDelete extends DialogFragment {

    private Pegawai pegawai;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle("Confirmation")
                .setMessage("Do you want to delete this item?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        onPositive();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });

        return builder.create();
    }

    public void setPegawai(Pegawai pegawai){
        this.pegawai = pegawai;
    }

    private void onPositive(){
        //String selection = SqliteHandler.COL_NIK + " = ?";
        //String[] selectionArgs = { this.pegawai.NIK };

        //MainActivity.sqLiteDatabase.delete(SqliteHandler.TBNAME, selection, selectionArgs);
        String query =
                "DELETE FROM " + SqliteHandler.TBNAME +
                " WHERE " + SqliteHandler.COL_NIK + " = " + this.pegawai.NIK;

        MainActivity.sqLiteDatabase.execSQL(query);

        MainActivity.listPegawai.remove(MainActivity.listPegawai.indexOf(this.pegawai));
        MainActivity.adapter.notifyDataSetChanged();
    }
}
