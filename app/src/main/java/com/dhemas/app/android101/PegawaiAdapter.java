package com.dhemas.app.android101;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Android101 by dhemas
 * created on: 29/03/2017
 */

public class PegawaiAdapter extends BaseAdapter {

    private Context context;
    private List<Pegawai> pegawaiList;
    private LayoutInflater inflater;

    public PegawaiAdapter(Context context, List<Pegawai> pegawaiList) {
        this.pegawaiList = pegawaiList;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return pegawaiList.size();
    }

    @Override
    public Object getItem(int i) {
        return pegawaiList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View rowView = inflater.inflate(R.layout.list_layout, viewGroup, false);
        TextView txt_nik = (TextView) rowView.findViewById(R.id.txt_nik);
        TextView txt_nama = (TextView) rowView.findViewById(R.id.txt_nama);

        final Pegawai pegawai = (Pegawai) getItem(i);
        txt_nik.setText(pegawai.NIK);
        txt_nama.setText(pegawai.NAMA);

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogDelete dialog = new DialogDelete();
                dialog.setPegawai(pegawai);
                dialog.show(((Activity) context).getFragmentManager() , "TAG");
            }
        });

        return rowView;
    }
}
